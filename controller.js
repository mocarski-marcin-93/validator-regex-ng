var app = angular.module('validation', []);

app.controller('validationCtrl', [function($scope) {


    this.errors = {
        name: '',
        email: '',
        tel: '',
        postalcode: ''
    }
    this.user = {
        name: '',
        email: '',
        tel: '',
        postalcode: ''
    };

    this.nameValidate = function() {
            var regex = /(\W|\d)/i;
            !regex.test(this.user.name) ? this.errors.name = "" : this.errors.name = "Invalid entry!";
        } // DONE!

    this.emailValidate = function() {
        var regex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        regex.test(this.user.email) ? this.errors.email = "" : this.errors.email = "Invalid entry!";
    }; // DONE!

    this.telValidate = function() {
        var regex = /(\D)/i;
        !regex.test(this.user.tel) ? this.errors.tel = "" : this.errors.tel = "Invalid entry!";
    }; // DONE?

    this.postalcodeValidate = function() {
        var regex = /[0-9]{2}-[0-9]{3}/i;
        regex.test(this.user.postalcode) ? this.errors.postalcode = "" : this.errors.postalcode = "Invalid entry!";

    }; // DONE!

    this.checkForEmptyFields = function() {
        var $x = $('input[data-validation*=required]').filter('.ng-empty'); // Zbieramy pola jednocześnie wymagane i puste
        var emptyFieldsCounter = 0;
        while($x[emptyFieldsCounter]) {
          switch ($x[emptyFieldsCounter].id) {
            case 'name':
                this.errors.name = 'Cannot be empty';
                break;
            case 'email':
                this.errors.email = 'Cannot be empty';
                break;
            case 'tel':
                this.errors.tel = 'Cannot be empty';
                break;
            case 'postalcode':
                this.errors.postalcode = 'Cannot be empty';
                break;
            default:
        }
        emptyFieldsCounter++;
      }
    }

    var _this = this;
    $('input[data-validation=submit]').on('click', function() {
        _this.checkForEmptyFields();
    })

}]);
