$(document).ready(function() {

    //Get form
    $('form[data-validation]').attr('ng-app', 'validation').attr('ng-controller', 'validationCtrl as ctrl');


    //Get name fields
    $('input[data-validation*=name]').attr('ng-model', 'ctrl.user.name').attr('ng-change', 'ctrl.nameValidate()').attr('id', 'name');
    $('.name').attr('ng-bind', 'ctrl.errors.name');

    // Get email fields
    $('input[data-validation*=email]').attr('ng-model', 'ctrl.user.email').attr('ng-change', 'ctrl.emailValidate()').attr('id', 'email');
    $('.email').attr('ng-bind', 'ctrl.errors.email');

    // Get tel fields
    $('input[data-validation*=tel]').attr('ng-model', 'ctrl.user.tel').attr('ng-change', 'ctrl.telValidate()').attr('id', 'tel');
    $('.tel').attr('ng-bind', 'ctrl.errors.tel');

    // Get postalcode fields
    $('input[data-validation*=postalcode]').attr('ng-model', 'ctrl.user.postalcode').attr('ng-change', 'ctrl.postalcodeValidate()').attr('id', 'postalcode');
    $('.postalcode').attr('ng-bind', 'ctrl.errors.postalcode');

    // Get required fields
    $('input[data-validation*=required]').before('* ').after(' *');
});
